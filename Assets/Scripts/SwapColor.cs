using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapColor : MonoBehaviour
{
    public Gradient Grad_SourceGradiente;
    private SpriteRenderer SR_Renderer;



    void Start()
    {
        SR_Renderer = GetComponent<SpriteRenderer>();
        SR_Renderer.color = Grad_SourceGradiente.Evaluate(Random.Range(0f, 1f));
    }
}
