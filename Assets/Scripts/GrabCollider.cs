using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabCollider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }

    private void OnMouseDrag()
    {
        Vector2 mousePos = Input.mousePosition;
        //Debug.Log(mousePos);
        Vector2 GrabPos = Camera.main.ScreenToWorldPoint(mousePos);
        Debug.Log(GrabPos);
        transform.position = GrabPos;
    }
}
