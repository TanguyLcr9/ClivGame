using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CursorBehavior : MonoBehaviour
{
    public Image coteGauche, coteDroit;
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public void UpdateColors(Color gauche,Color droite)
    {
        coteGauche.color = gauche;
        coteDroit.color = droite;
    }
    // Update is called once per frame
    public void UpdateAngle(float graduation)
    {
        transform.localEulerAngles = new Vector3(0, 0, graduation * 80f);
    }
}
