using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class EtiquetteData {
    public string name;
    public Color col;

    public void SetEtiquette(string newName, Color newColor)
    {
        name = newName;
        col = newColor;
    }
}

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public GameObject Menu;
    public List<GameObject> miniJeux;
    public float timerMax, timerBonusMax;
    public CursorBehavior cursor;
    public Etiquette etiquetteJ1, etiquetteJ2;
    public Animator UIAnimator;
    public List<EtiquetteData> EtiquetteDataJ1, EtiquetteDataJ2;

    bool ChoiseReversed;
    bool TimerPlay;
    float graduation;
    public float ScoreJGauche, ScoreJDroite;
    float timer;
    int minijeuCount;
    GameObject actualMiniJeu;
    public Animator Animator_Transition;

    [Header("UI")]
    public Text timerDisplay;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        TimerPlay = false;
        timerDisplay.text = "";
        cursor.transform.parent.gameObject.SetActive(false);

        actualMiniJeu = Instantiate(Menu);
        //StartRound();

    }

    public void StartRound()
    {
        cursor.transform.parent.gameObject.SetActive(false);
        Destroy(actualMiniJeu);
        graduation = 0;
        actualMiniJeu = Instantiate(miniJeux[minijeuCount]);
        minijeuCount++;
        if(minijeuCount >= miniJeux.Count)
        {
            minijeuCount = 0;
        }
        if (actualMiniJeu.CompareTag("Bonus"))//Dans le cas ou c'est un niveau bonus
        {
            TimerPlay = true;
            timer = timerBonusMax;
            graduation = 0;
        }
        //TimerPlay = true;
    }

    public void StartMiniGame()
    {
        cursor.transform.parent.gameObject.SetActive(true);
        graduation = 0;
        TimerPlay = true;
        timer = timerMax;
        actualMiniJeu.GetComponent<MiniGameManager>().ActivateGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            PlayTransition();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (TimerPlay)
        {
            timer -= Time.deltaTime;
            timerDisplay.text = ((int)timer+1)+"''";
            if (timer <= 0)
            {
                Debug.Log("Stop the fight");
                TimerPlay = false;
                timerDisplay.text = "";
                timer = timerMax;
                //undisplay etiquettes
                UIAnimator.SetBool("Display", false);
                //Scoring
                if(graduation > 0.1f)
                {
                    ScoreJGauche += 1;
                }
                else if(graduation < -0.1f)
                {
                    
                    ScoreJDroite += 1;
                }

                PlayTransition();
            }
        }
    }

    public void PlayTransition()
    {
        Animator_Transition.Play("Next");
    }

    public void SetEtiquette(string joueur, string S_avec, Color C_avec, string S_sans, Color C_sans)
    {
        //display etiquettes
        UIAnimator.SetBool("Display", true);
        EtiquetteData dataJ1 = new EtiquetteData();
        EtiquetteData dataJ2 = new EtiquetteData();

        if (joueur == "Joueur 1")
        {
            ChoiseReversed = false;
            etiquetteJ2.DisplayEtiquette(S_avec, C_avec);
            etiquetteJ1.DisplayEtiquette(S_sans, C_sans);
            cursor.UpdateColors(C_avec, C_sans);
            
            dataJ1.SetEtiquette(S_sans, C_sans);
            dataJ2.SetEtiquette(S_avec, C_avec);
            EtiquetteDataJ1.Add(dataJ1);
            EtiquetteDataJ2.Add(dataJ2);
        }
        if (joueur == "Joueur 2")
        {
            ChoiseReversed = true;
            etiquetteJ2.DisplayEtiquette(S_sans, C_sans);
            etiquetteJ1.DisplayEtiquette(S_avec, C_avec);
            cursor.UpdateColors(C_sans, C_avec);

            dataJ1.SetEtiquette(S_avec, C_avec);
            dataJ2.SetEtiquette(S_sans, C_sans);
            EtiquetteDataJ1.Add(dataJ1);
            EtiquetteDataJ2.Add(dataJ2);
        }

        StartMiniGame();
    }

    public void UpdateGraduation(float newGraduation)
    {
        graduation = newGraduation;
        if (ChoiseReversed)
            graduation = -graduation;
        cursor.UpdateAngle(graduation);
    }
}
