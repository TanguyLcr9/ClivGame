using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class Restartclose : MonoBehaviour
{
    public Text scoreDisplay1;
    public Text scoreDisplay2;
    public Text winner;

    // Start is called before the first frame update
    void Start()
    {
        
        ScoringFinal((int)GameManager.instance.ScoreJGauche, (int)GameManager.instance.ScoreJDroite);
    }

    // Update is called once per frame
    void Update()
    {
            if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("MenuMathou");
        }             
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();        
        } 
    }

    public void ScoringFinal(int score1, int score2)
    {
        scoreDisplay1.text = "Score J1\n " + score1;
        scoreDisplay2.text = "Score J2\n" + score2;
        if (score1 > score2)
        {
            winner.text = "Le Gagnant est : Joueur 1";
        }
        
        if (score2 > score1)
        {
            winner.text = "Le Gagnant est : Joueur 2";
        }
        
        if (score2 == score1)
        {
            winner.text = "ÉGALITÉ !!!";
        }
    }
}
