using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PundingBehaviour : MonoBehaviour
{
    AudioSource source;
    AudioSource source2;
    public AudioClip flambonau, slap;
    public GameObject GO_Spark;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        source2 = gameObject.AddComponent<AudioSource>();
        source2.playOnAwake = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<Animator>().SetTrigger("Splotch");
        source.clip = flambonau;
        source.Play();
        Instantiate(GO_Spark, new Vector3(collision.GetContact(0).point.x, collision.GetContact(0).point.y, 0), Quaternion.identity);
        source2.clip = slap;
        source2.Play();
    }

}
