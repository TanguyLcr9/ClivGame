using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallonManager : MonoBehaviour
{

    public GameObject BallonRight, BallonLeft;
    public float rangeBetween;


    // Update is called once per frame
    void Update()
    {
        rangeBetween = BallonRight.transform.localScale.y - BallonLeft.transform.localScale.y;
        GameManager.instance.UpdateGraduation(rangeBetween);
    }
}
