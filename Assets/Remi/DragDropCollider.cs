using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDropCollider : MonoBehaviour 
{
    public GameObject GO_HandDeplacement;
    public GameObject GO_Object;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GO_HandDeplacement.GetComponent<HandDeplacement>().B_Grab == false)
        {
            if (collision.CompareTag("Object"))
            {
                GO_Object = collision.gameObject;
            }
        }
    }

   private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Object"))
        {           
            GO_Object = null;
        }
    }
}
