using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectScript : MonoBehaviour
{
    public GameObject GO_Hand;
    private Vector3 V3_Offset;
    public bool B_Taken = false;
    public UnityEvent OnDrag;

    public void TakeHand(GameObject GO_pos)
    {
        GO_Hand = GO_pos;
        V3_Offset = gameObject.transform.position - GO_pos.transform.position;
        B_Taken = true;
        OnDrag.Invoke();
    }

    private void Update()
    {
        if (B_Taken == true)
        {
            transform.position = GO_Hand.transform.position + V3_Offset;
        }
    }
}

