using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPosition : MonoBehaviour
{
    public Transform T_Pivot;

    private void Update()
    {
        transform.position = T_Pivot.position;
    }
}
