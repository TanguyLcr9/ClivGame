using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ClampObject : MonoBehaviour
{
    [MinMaxSlider(-15f, 15f)]
    public Vector2 V2_X;

    [MinMaxSlider(-15f, 15f)]
    public Vector2 V2_Y;

    private void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, V2_X.x, V2_X.y), Mathf.Clamp(transform.position.y, V2_Y.x, V2_Y.y),transform.position.z);
    }
}
