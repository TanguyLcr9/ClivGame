using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockRotation : MonoBehaviour
{
    private Quaternion Qua_BasePos;

    private void Start()
    {
        Qua_BasePos = transform.localRotation;
    }

    private void Update()
    {
        transform.localRotation = Qua_BasePos;
    }
}
