using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour
{
    public GameObject GO_Shadow;
    public GameObject GO_light;
    public GameObject GO_HandPivot;
    public bool B_On = true;

    public void SwitchLight()
    {
        if (B_On == true)
        {
            GO_Shadow.SetActive(true);
            GO_light.SetActive(false);
            GameManager.instance.UpdateGraduation(-1);
            B_On = false;
        }
        else
        {
            GO_Shadow.SetActive(false);
            GO_light.SetActive(true);
            GameManager.instance.UpdateGraduation(1);
            B_On = true;
        }
    }

    private void Update()
    {
        transform.position = GO_HandPivot.transform.position + (Vector3.ClampMagnitude(transform.position - GO_HandPivot.transform.position, 7));
    }
}
