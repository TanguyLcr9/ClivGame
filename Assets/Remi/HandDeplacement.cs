using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class HandDeplacement : MonoBehaviour
{
    [Dropdown("S_Joueur")]
    public string S_Player;

    private List<string> S_Joueur { get { return new List<string>() { "Joueur 1", "Joueur 2" }; } }

    public int I_Velocity = 10;
    public GameObject GO_HandPivot;
    private Vector3 V3_Velocity;
    private Vector3 V3_Clamp;

    public GameObject GO_HandCollider;
    public bool B_Grab = false;
    public bool B_Punch = false;

    public GameObject GO_Animator_Hand;
    private Animator Animator_Hand;
    public AudioSource AS_Sound;
    public AudioClip AC_take;
    public AudioClip AC_Slap1;
    public AudioClip AC_Slap2;
    public GameObject GO_Spark;

    private void Start()
    {
        Animator_Hand = GO_Animator_Hand.GetComponent<Animator>();
    }

    void Update()
    {
        if (S_Player == "Joueur 1")
        {
            V3_Velocity = (Vector3.left * Input.GetAxis("Vertical p1") * I_Velocity * Time.deltaTime) + (Vector3.up * Input.GetAxis("Horizontal p1") * I_Velocity * Time.deltaTime);
        }
        else
        {
            V3_Velocity = (Vector3.left * Input.GetAxis("Vertical p2") * I_Velocity * Time.deltaTime) + (Vector3.up * Input.GetAxis("Horizontal p2") * -I_Velocity * Time.deltaTime);
        }

         
        transform.Translate(V3_Velocity);

        V3_Clamp = Vector3.ClampMagnitude(transform.position - GO_HandPivot.transform.position, 14);
        transform.position = GO_HandPivot.transform.position + V3_Clamp;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x,-9,9), Mathf.Clamp(transform.position.y, -3, 6), transform.position.z);

        if (S_Player == "Joueur 1")
        {
            if (Input.GetButton("Fire1 p1"))
            {
                Animator_Hand.Play("Close Hand");
                if (GO_HandCollider.GetComponent<DragDropCollider>().GO_Object != null && B_Punch == false && GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().B_Taken == false)
                {
                    GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().TakeHand(gameObject); 
                    B_Grab = true;
                    AS_Sound.clip = AC_take;
                    AS_Sound.Play();
                }
                else
                {
                    B_Punch = true;
                }

            }
            else
            {
                Animator_Hand.Play("Open Hand");

                if (GO_HandCollider.GetComponent<DragDropCollider>().GO_Object != null)
                {
                    GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().B_Taken = false;
                }
                B_Grab = false;
                B_Punch = false;
            }
        }
        else
        {
            if (Input.GetButton("Fire1 p2"))
            {
                Animator_Hand.Play("Close Hand");
                if (GO_HandCollider.GetComponent<DragDropCollider>().GO_Object != null && B_Punch == false && GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().B_Taken == false)
                {
                    GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().TakeHand(gameObject);
                    B_Grab = true;
                    AS_Sound.clip = AC_take;
                    AS_Sound.Play();
                }
                else
                {
                    B_Punch = true;
                }

            }
            else
            {
                Animator_Hand.Play("Open Hand");

                if (GO_HandCollider.GetComponent<DragDropCollider>().GO_Object != null)
                {
                    GO_HandCollider.GetComponent<DragDropCollider>().GO_Object.GetComponent<ObjectScript>().B_Taken = false;
                }
                B_Grab = false;
                B_Punch = false;
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (Random.Range(0,1) >=1)
            {
                AS_Sound.clip = AC_Slap1;
            }
            else
            {
                AS_Sound.clip = AC_Slap2;
            }
            
            AS_Sound.Play();

            Instantiate(GO_Spark, new Vector3(collision.GetContact(0).point.x, collision.GetContact(0).point.y, 0), Quaternion.identity);
        }
    }
}
