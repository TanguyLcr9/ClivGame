using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameManager : MonoBehaviour
{
    AudioSource source;

    public GameObject Selection, Game;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        Selection.SetActive(true);
        Game.SetActive(false);
    }

    public void ActivateGame()
    {
        Selection.SetActive(false);
        Game.SetActive(true);
        source.Play(); //Son déclenchement du mini jeu
    }
}
