using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EtiquetteManager : MonoBehaviour
{

    public ObjectScript etiquetteAvec;
    public ObjectScript etiquetteSans;

    public void EtiquetteSelection(bool avec)
    {
        string S_avec = transform.GetChild(0).GetChild(1).GetComponent<TextMesh>().text;
        Color C_avec = transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().color;
        string S_sans = transform.GetChild(1).GetChild(1).GetComponent<TextMesh>().text;
        Color C_sans = transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().color;
        Debug.Log(S_avec);

        //Debug.Log("Dragge");
        if (avec)
        {
            if (etiquetteAvec.GO_Hand.GetComponent<HandDeplacement>().S_Player == "Joueur 1")
            {
                GameManager.instance.SetEtiquette("Joueur 2", S_avec, C_avec, S_sans, C_sans);
            }
            else
            {
                GameManager.instance.SetEtiquette("Joueur 1", S_avec, C_avec, S_sans, C_sans);
            }
            //Debug.Log(etiquetteAvec.GO_Hand.GetComponent<HandDeplacement>().S_Player);
            //Debug.Log(avec);
        }
        else
        {
            GameManager.instance.SetEtiquette(etiquetteSans.GO_Hand.GetComponent<HandDeplacement>().S_Player, S_avec, C_avec, S_sans, C_sans);
            
        }

    }
}
