using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EtiquetteChoixBehavior : MonoBehaviour
{
    AudioSource source;
    public AudioClip TombeTable, BruitPain;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if(collision.gameObject.name == "Table")
        {
            source.clip = TombeTable;
            source.Play();
        }
    }
}
