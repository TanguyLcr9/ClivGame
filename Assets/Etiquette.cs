using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Etiquette : MonoBehaviour
{
    public Text Texte;
    public Image etiquetteImage;

    public void DisplayEtiquette(string text, Color col)
    {
        Texte.text = text;
        etiquetteImage.color = col;
    }
}
