using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehavior : MonoBehaviour
{
    
    public GameObject buttonPressed, buttonReleased;
    public GameObject ballon;
    public GameObject ballonExplosion;
    public float scalingspeed;
    public float decreasingSpeed;
    public AudioClip gonfle, pop, clicOn, clicOff; 
    public AudioSource ButonSource;
    public AudioSource ballonSource;
    public float TimerReset;

    bool souffle, ballonExplose;
    float timer;
    float scaleBase, scaleMax = 1.2f;

    private void Start()
    {
        buttonReleased.SetActive(true);
        buttonPressed.SetActive(false);

        scaleBase = ballon.transform.localScale.y;

    }

    private void Update()
    {
        if (ballonExplose)
        {
            TimerReset += Time.deltaTime;
            if (TimerReset >= 2)
            {
                ballon.SetActive(true);
                ballonExplose = false;
                ballon.transform.localScale = new Vector3(scaleBase, scaleBase, scaleBase);
                TimerReset = 0;
            }
            return;
        }   

        if (souffle)
        {
            timer += Time.deltaTime;
            ballon.transform.localScale += Vector3.up * Time.deltaTime * scalingspeed;
            ballon.transform.localScale += Vector3.right * Time.deltaTime * scalingspeed *0.8f;
            if(ballon.transform.localScale.y >= scaleMax)
            {
                //explosion
                Instantiate(ballonExplosion, ballon.transform.position ,Quaternion.identity,null);
                ballon.SetActive(false);
                ballonExplose = true;
                ballon.transform.localScale = new Vector3(scaleBase, scaleBase, scaleBase);
                ballonSource.clip = pop;
                ballonSource.Play();
            }

            if (timer >= 0.15f)
            {
                timer = 0;
                souffle = false;
            }
        }
        else
        {
            if(ballonExplose == false &&  ballon.transform.localScale.y >= scaleBase)
            {
                ballon.transform.localScale -= Vector3.up * Time.deltaTime * decreasingSpeed;
                ballon.transform.localScale -= Vector3.right * Time.deltaTime * decreasingSpeed * 0.8f;
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            buttonReleased.SetActive(false);
            buttonPressed.SetActive(true);
            souffle = true;
            timer = 0;
            ButonSource.clip = clicOn;
            ButonSource.Play();
            if (!ballonExplose)
            {
                ballonSource.clip = gonfle;
                ballonSource.Play();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            buttonReleased.SetActive(true);
            buttonPressed.SetActive(false);
            ButonSource.clip = clicOff;
            ButonSource.Play();
        }
    }
}
