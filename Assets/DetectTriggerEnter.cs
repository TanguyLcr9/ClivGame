using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectTriggerEnter : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Object"))
        {
            GameManager.instance.UpdateGraduation(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Object"))
        {
            GameManager.instance.UpdateGraduation(-1);
        }
    }
}
