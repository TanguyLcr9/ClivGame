using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainAuChocBehaviour : MonoBehaviour
{
    bool painAuchoc, Chocolatine;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "EtiquettePainauChocolat")
        {
            painAuchoc = true;
        }

        if(collision.name == "EtiquetteChocolatine")
        {
            Chocolatine = true;
        }
        Calculate();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "EtiquettePainauChocolat")
        {
            painAuchoc = false;
        }

        if (collision.name == "EtiquetteChocolatine")
        {
            Chocolatine = false;
        }
        Calculate();
    }

    public void Calculate()
    {
        float value = 0;
        if (painAuchoc)
        {
            value += 1;
        }
        if (Chocolatine)
        {
            value -= 1;
        }

        GameManager.instance.UpdateGraduation(value);
    }
}
